#!/usr/bin/env ruby

require 'mysql2'
require 'yaml'

begin
  require 'active_support'
  require 'active_support/core_ext'
rescue LoadError
end

config = YAML.load(File.read('database.yml'))
cli = Mysql2::Client.new(config['staging'])
cli.query(<<-SQL)
SELECT
  `immeubles`.`id` AS t0_r0
, `immeubles`.`code` AS t0_r1
, `immeubles`.`contrat_id` AS t0_r2
, `immeubles`.`civilite_id` AS t0_r3
, `immeubles`.`nom` AS t0_r4
, `immeubles`.`adresse` AS t0_r5
, `immeubles`.`code_postal` AS t0_r6
, `immeubles`.`ville` AS t0_r7
, `immeubles`.`pays` AS t0_r8
, `immeubles`.`nombre_de_pieces` AS t0_r9
, `immeubles`.`surface_developpee` AS t0_r10
, `immeubles`.`surface_du_terrain` AS t0_r11
, `immeubles`.`created_at` AS t0_r12
, `immeubles`.`updated_at` AS t0_r13
, `immeubles`.`ext` AS t0_r14
, `immeubles`.`date_entree` AS t0_r15
, `immeubles`.`date_sortie` AS t0_r16
, `immeubles`.`date_debut_bail` AS t0_r17
, `immeubles`.`numero_appartement` AS t0_r18
, `immeubles`.`etage` AS t0_r19
, `immeubles`.`colocation` AS t0_r20
, `immeubles`.`uuid` AS t0_r21
, `immeubles`.`type_de_bien_id` AS t0_r22
, `immeubles`.`activite_principale` AS t0_r23
, `immeubles`.`date_effet_mandat_gestion` AS t0_r24
, `immeubles`.`motif_resiliation_id` AS t0_r25
, `immeubles`.`utilisateur_id` AS t0_r26
, `immeubles`.`numero_immeuble` AS t0_r27
, `immeubles`.`prime_ttc` AS t0_r28
, `immeubles`.`frais` AS t0_r29
, `immeubles`.`date_construction` AS t0_r30
, `immeubles`.`concierge` AS t0_r31
, `immeubles`.`notes` AS t0_r32
, `immeubles`.`regroupement1_id` AS t0_r33
, `immeubles`.`regroupement2_id` AS t0_r34
, `immeubles`.`usage_id` AS t0_r35
, `immeubles`.`nature_id` AS t0_r36
, `immeubles`.`activite_type_id` AS t0_r37
, `contrats`.`id` AS t1_r0
, `contrats`.`client_id` AS t1_r1
, `contrats`.`code` AS t1_r2
, `contrats`.`police_compagnie` AS t1_r3
, `contrats`.`compagnie_id` AS t1_r4
, `contrats`.`branche_id` AS t1_r5
, `contrats`.`categorie_id` AS t1_r6
-- , `contrats`.`fractionnement_id` AS t1_r7
-- , `contrats`.`etat_id` AS t1_r8
-- , `contrats`.`date_effet` AS t1_r9
-- , `contrats`.`date_fin` AS t1_r10
-- , `contrats`.`prochaine_echeance` AS t1_r11
-- , `contrats`.`date_creation` AS t1_r12
-- , `contrats`.`utilisateur_creation` AS t1_r13
-- , `contrats`.`jour_echeance` AS t1_r14
-- , `contrats`.`mois_echeance` AS t1_r15
-- , `contrats`.`gestionnaire_id` AS t1_r16
-- , `contrats`.`note` AS t1_r17
-- , `contrats`.`apporteur_code` AS t1_r18
-- , `contrats`.`situation_id` AS t1_r19
-- , `contrats`.`date_demande` AS t1_r20
-- , `contrats`.`created_at` AS t1_r21
-- , `contrats`.`updated_at` AS t1_r22
-- , `contrats`.`sinistre_gestionnaire_id` AS t1_r23
-- , `contrats`.`devise_id` AS t1_r24
-- , `contrats`.`sortie_vehicule_nombre_jours_avant` AS t1_r25
-- , `contrats`.`carte_verte_defaut_adresse_type_id` AS t1_r26
-- , `contrats`.`ante` AS t1_r27
-- , `contrats`.`ext` AS t1_r28
-- , `contrats`.`numero_contrat_groupe` AS t1_r29
-- , `contrats`.`numero_contrat_courtier` AS t1_r30
-- , `contrats`.`portefeuille_id` AS t1_r31
-- , `contrats`.`mandat_id` AS t1_r32
-- , `contrats`.`uuid` AS t1_r33
-- , `contrats`.`point_de_vente_code` AS t1_r34
-- , `contrats`.`produit_code` AS t1_r35
-- , `contrats`.`groupe_contrat_entete_id` AS t1_r36
-- , `contrats`.`compagnie_garantie_formule_id` AS t1_r37
, `contrats`.`contrat_modele_id` AS t1_r38
FROM `immeubles`
INNER JOIN `contrats` ON `contrats`.`id` = `immeubles`.`contrat_id`
WHERE `immeubles`.`contrat_id` = 16042
SQL
