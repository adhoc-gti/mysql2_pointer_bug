-- MySQL dump 10.16  Distrib 10.1.20-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: localhost
-- ------------------------------------------------------
-- Server version	10.1.20-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aeronef_pieces`
--

DROP TABLE IF EXISTS `contrats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `police_compagnie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `compagnie_id` int(11) DEFAULT NULL,
  `branche_id` int(11) DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `fractionnement_id` int(11) DEFAULT NULL,
  `etat_id` int(11) DEFAULT NULL,
  `date_effet` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `prochaine_echeance` date DEFAULT NULL,
  `date_creation` datetime DEFAULT NULL,
  `utilisateur_creation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jour_echeance` int(11) DEFAULT NULL,
  `mois_echeance` int(11) DEFAULT NULL,
  `gestionnaire_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `apporteur_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `situation_id` int(11) DEFAULT NULL,
  `date_demande` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sinistre_gestionnaire_id` int(11) DEFAULT NULL,
  `devise_id` int(11) DEFAULT NULL,
  `sortie_vehicule_nombre_jours_avant` int(11) DEFAULT NULL,
  `carte_verte_defaut_adresse_type_id` int(11) DEFAULT NULL,
  `ante` tinyint(1) DEFAULT NULL,
  `ext` text COLLATE utf8_unicode_ci,
  `numero_contrat_groupe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_contrat_courtier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `portefeuille_id` int(11) DEFAULT NULL,
  `mandat_id` int(11) DEFAULT NULL,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `point_de_vente_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `produit_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `groupe_contrat_entete_id` int(11) DEFAULT NULL,
  `compagnie_garantie_formule_id` int(11) DEFAULT NULL,
  `contrat_modele_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_contrats_on_code` (`code`),
  KEY `index_contrats_on_uuid` (`uuid`),
  KEY `index_contrats_on_contrat_modele_id` (`contrat_modele_id`),
  KEY `index_contrats_on_client_id` (`client_id`),
  KEY `index_contrats_on_compagnie_id` (`compagnie_id`),
  KEY `index_contrats_on_branche_id` (`branche_id`),
  KEY `index_contrats_on_categorie_id` (`categorie_id`),
  KEY `index_contrats_on_fractionnement_id` (`fractionnement_id`),
  KEY `index_contrats_on_etat_id` (`etat_id`),
  KEY `index_contrats_on_date_effet` (`date_effet`),
  KEY `index_contrats_on_date_fin` (`date_fin`),
  KEY `index_contrats_on_situation_id` (`situation_id`),
  KEY `index_contrats_on_ante` (`ante`),
  KEY `index_contrats_on_gestionnaire_id` (`gestionnaire_id`),
  KEY `index_contrats_on_sinistre_gestionnaire_id` (`sinistre_gestionnaire_id`),
  KEY `index_contrats_on_devise_id` (`devise_id`),
  KEY `index_contrats_on_mandat_id` (`mandat_id`),
  KEY `index_contrats_on_compagnie_garantie_formule_id` (`compagnie_garantie_formule_id`),
  KEY `index_contrats_on_groupe_contrat_entete_id` (`groupe_contrat_entete_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrats`
--

LOCK TABLES `contrats` WRITE;
/*!40000 ALTER TABLE `contrats` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrats` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `immeubles`
--

DROP TABLE IF EXISTS `immeubles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `immeubles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contrat_id` int(11) DEFAULT NULL,
  `civilite_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_postal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pays` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_de_pieces` int(11) DEFAULT NULL,
  `surface_developpee` decimal(14,2) DEFAULT NULL,
  `surface_du_terrain` decimal(14,2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ext` text COLLATE utf8_unicode_ci,
  `date_entree` date DEFAULT NULL,
  `date_sortie` date DEFAULT NULL,
  `date_debut_bail` date DEFAULT NULL,
  `numero_appartement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `etage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colocation` tinyint(1) DEFAULT NULL,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_de_bien_id` int(11) DEFAULT NULL,
  `activite_principale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_effet_mandat_gestion` date DEFAULT NULL,
  `motif_resiliation_id` int(11) DEFAULT NULL,
  `utilisateur_id` int(11) DEFAULT NULL,
  `numero_immeuble` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prime_ttc` decimal(14,2) DEFAULT NULL,
  `frais` decimal(14,2) DEFAULT NULL,
  `date_construction` date DEFAULT NULL,
  `concierge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `regroupement1_id` int(11) DEFAULT NULL,
  `regroupement2_id` int(11) DEFAULT NULL,
  `usage_id` int(11) DEFAULT NULL,
  `nature_id` int(11) DEFAULT NULL,
  `activite_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_immeubles_on_contrat_id` (`contrat_id`),
  KEY `index_immeubles_on_uuid` (`uuid`),
  KEY `index_immeubles_on_code` (`code`),
  KEY `index_immeubles_on_civilite_id` (`civilite_id`),
  KEY `index_immeubles_on_date_entree` (`date_entree`),
  KEY `index_immeubles_on_date_sortie` (`date_sortie`),
  KEY `index_immeubles_on_regroupement1_id` (`regroupement1_id`),
  KEY `index_immeubles_on_regroupement2_id` (`regroupement2_id`),
  KEY `index_immeubles_on_utilisateur_id` (`utilisateur_id`),
  KEY `index_immeubles_on_nature_id` (`nature_id`),
  KEY `index_immeubles_on_usage_id` (`usage_id`),
  KEY `index_immeubles_on_activite_type_id` (`activite_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `immeubles`
--

LOCK TABLES `immeubles` WRITE;
/*!40000 ALTER TABLE `immeubles` DISABLE KEYS */;
/*!40000 ALTER TABLE `immeubles` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-17  9:00:12
