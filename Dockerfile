FROM debian:8.6
LABEL vendor="ADHOC-GTI"

RUN apt-get update
RUN env DEBIAN_FRONTEND=noninteractive apt-get install -y mariadb-server libmariadb-client-lgpl-dev libmariadb-client-lgpl-dev-compat
RUN apt-get install -y curl
RUN apt-get install -y autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev
RUN mkdir /tmp/ruby && cd /tmp/ruby && curl --remote-name --progress https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.3.tar.gz
RUN cd /tmp/ruby && echo '1014ee699071aa2ddd501907d18cbe15399c997d  ruby-2.3.3.tar.gz' | shasum -c - && tar xzf ruby-2.3.3.tar.gz
RUN cd /tmp/ruby && cd ruby-2.3.3 && ./configure --disable-install-rdoc && make && make install && ruby --version
RUN gem install bundler --no-ri --no-rdoc

ADD Gemfile.* /home/test/

RUN cd /home/test && BUNDLE_GEMFILE=Gemfile.norails bundle install --path bundle
RUN cd /home/test && BUNDLE_GEMFILE=Gemfile.rails3 bundle install --path bundle
RUN cd /home/test && BUNDLE_GEMFILE=Gemfile.rails4 bundle install --path bundle
RUN cd /home/test && BUNDLE_GEMFILE=Gemfile.rails5 bundle install --path bundle

ADD test.rb /home/test/
ADD database.yml /home/test/
ADD database.sql /home/test/
ADD run.sh /home/test/

ENTRYPOINT ["/home/test/run.sh"]
