#!/bin/bash

set -e
set -u
set -o pipefail

function handler() {
    echo "interrupted!"
    exit 1
}
trap handler INT

echo "starting mysql..."
nohup mysqld_safe 2>/dev/null &
sleep 3

cd /home/test

db_names=(
    a
    ab
    abc
    abcd
    abcde
    abcdef
    abcdefg
    abcdefgh
    abcdefghi
    abcdefghij
    abcdefghijk
    abcdefghijkl
    abcdefghijklm
    abcdefghijklmn
    abcdefghijklmno
    abcdefghijklmnop
    abcdefghijklmnopq
    abcdefghijklmnopqr
    abcdefghijklmnopqrs
    abcdefghijklmnopqrst
    abcdefghijklmnopqrstu
    abcdefghijklmnopqrstuv
    abcdefghijklmnopqrstuvw
    abcdefghijklmnopqrstuvwx
    abcdefghijklmnopqrstuvwxy
    abcdefghijklmnopqrstuvwxyz
    abcdefghijklmnopqrstuvwxyza
    abcdefghijklmnopqrstuvwxyzab
    abcdefghijklmnopqrstuvwxyzabc
    abcdefghijklmnopqrstuvwxyzabcd
    abcdefghijklmnopqrstuvwxyzabcde
    abcdefghijklmnopqrstuvwxyzabcdef
    abcdefghijklmnopqrstuvwxyzabcdefg
    abcdefghijklmnopqrstuvwxyzabcdefgh
    abcdefghijklmnopqrstuvwxyzabcdefghi
    abcdefghijklmnopqrstuvwxyzabcdefghij
    abcdefghijklmnopqrstuvwxyzabcdefghijk
    abcdefghijklmnopqrstuvwxyzabcdefghijkl
    abcdefghijklmnopqrstuvwxyzabcdefghijklm
    abcdefghijklmnopqrstuvwxyzabcdefghijklmn
    abcdefghijklmnopqrstuvwxyzabcdefghijklmno
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnop
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopq
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqr
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrs
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrst
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstu
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvw
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwx
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy
    abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
)

for db in ${db_names[@]}; do
    echo "** ${#db} $db"
    mysql -e "create database $db"
    mysql "$db" < database.sql
    sed -i -e "s/database: .*$/database: '$db'/" database.yml
    for v in norails rails3 rails4 rails5; do
        echo "*** $v"
        export BUNDLE_GEMFILE=Gemfile.$v
        for i in $(seq 1 20); do
            echo -n '.'
            if ! bundle exec ruby test.rb 2>&1; then
                echo
                echo "!!!! failed: ${#db} $db $v" 1>&2
                break
            fi
        done
        echo
    done
done
