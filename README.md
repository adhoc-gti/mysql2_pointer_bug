# mysql2 pointer bug

This project reproduces a bug in mysql2 / MariaDB / activesupport that triggers
on specific SQL queries when the database name length is 25.

This is reproducible on various ruby versions, with mysql2 0.3 and 0.4 as well
as activesupport 5.0, 4.2 and 3.2.

A Dockerfile is provided for ease of reproduction, so you juste have to run:

```
docker build .
docker run --rm -it decafbad
```

This tests a db name length of 24 (ok), 25 (fails) and 26 (ok), ten times each
since sometimes the bug doesn't trigger.
